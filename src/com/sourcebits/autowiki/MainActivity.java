package com.sourcebits.autowiki;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends Activity {
	private String data;
	private List<String> suggest;
	private AutoCompleteTextView autoComplete;
	private ArrayAdapter<String> aAdapter;
	private WebView web;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		web = (WebView) findViewById(R.id.webView);
		suggest = new ArrayList<String>();
		autoComplete = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
		autoComplete.addTextChangedListener(new TextWatcher() {

			private String newText;

			public void afterTextChanged(Editable editable) {
				web.setWebViewClient(new myWebClient());
				web.getSettings().setJavaScriptEnabled(true);
				web.getSettings().setSupportZoom(true);
				web.getSettings().setBuiltInZoomControls(true);
				web.loadUrl("http://simple.wikipedia.org/wiki/" + newText);
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				newText = s.toString();

				new getJson().execute(newText);
			}

		});

	}

	public class myWebClient extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {

			super.onPageStarted(view, url, favicon);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);

		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && web.canGoBack()) {
			web.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	class getJson extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... key) {
			String newText = key[0];
			newText = newText.trim();
			newText = newText.replace(" ", "+");
			try {
				HttpClient hClient = new DefaultHttpClient();
				HttpGet hGet = new HttpGet(
						"http://en.wikipedia.org/w/api.php?action=opensearch&search="
								+ newText + "&limit=8&namespace=0&format=json");
				ResponseHandler<String> rHandler = new BasicResponseHandler();
				data = hClient.execute(hGet, rHandler);
				suggest = new ArrayList<String>();
				JSONArray jArray = new JSONArray(data);
				for (int i = 0; i < jArray.getJSONArray(1).length(); i++) {
					String SuggestKey = jArray.getJSONArray(1).getString(i);
					suggest.add(SuggestKey);
				}

			} catch (Exception e) {
				Log.w("Error", e.getMessage());
			}
			runOnUiThread(new Runnable() {
				public void run() {
					aAdapter = new ArrayAdapter<String>(
							getApplicationContext(), R.layout.item, suggest);
					autoComplete.setAdapter(aAdapter);
					aAdapter.notifyDataSetChanged();
				}
			});

			return null;
		}
	}
}